import {BehaviorSubject, Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {OrderProduct} from '../models/orderProduct.model';
import {OrderModel} from '../models/Order.model';
import {environment} from '../../environments/environment';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class ShoppingCartService {
  private shoppingCartItems = [];
  private shoppingCartItemsSubject = new BehaviorSubject<OrderProduct[]>([]);
  private orderUrl = environment.apiUrl + 'order';
  private cookieValue: string;

  constructor(private http: HttpClient, private authService: AuthService, private cookieService: CookieService) { }


  loadCookie() {
    this.shoppingCartItems = JSON.parse(this.cookieService.get('cart-cookie'))
    this.updateShoppingCartObservable();
  }


  addToShoppingcart(orderProduct: OrderProduct) {
    if (this.shoppingCartItems.some(item => item.product.id === orderProduct.product.id)) {
      this.shoppingCartItems.find(item => item.product.id === orderProduct.product.id).amount += orderProduct.amount;
    } else {
      this.shoppingCartItems.push(orderProduct);
    }
    this.updateShoppingCartObservable();
  }

  removeFromShoppingcart(productId: number) {
    if (this.shoppingCartItems.find(item => item.product.id === productId).amount > 1) {
      this.shoppingCartItems.find(item => item.product.id === productId).amount -= 1;
    } else if(this.shoppingCartItems.find(item => item.product.id === productId).amount <= 1) {
      this.shoppingCartItems = this.shoppingCartItems.filter(item => item.product.id !== productId)
    }
    this.updateShoppingCartObservable();
  }
  emptyShoppingCart() {
    this.shoppingCartItems = [];
    this.updateShoppingCartObservable();
  }

  getShoppingCartObservable() {
    return this.shoppingCartItemsSubject.asObservable();
  }
  updateShoppingCartObservable() {
    this.shoppingCartItemsSubject.next(this.shoppingCartItems)
    this.cookieValue = JSON.stringify(this.shoppingCartItems)
    this.cookieService.set('cart-cookie', this.cookieValue);
  }

  placeOrder(shoppingCartItems: OrderProduct[]): Observable<any> {
    return this.http.post(this.orderUrl + '/' + this.authService.getAttributeFromToken('email'), shoppingCartItems, {responseType: 'json'});
  }

  getOrders(username): Observable<OrderModel[]> {
    return this.http.get<OrderModel[]>(this.orderUrl + '/' + username, {responseType: 'json'})
  }
}
