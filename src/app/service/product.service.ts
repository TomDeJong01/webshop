import {Injectable} from '@angular/core';
import {Product} from '../models/product.model';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../environments/environment";

@Injectable()
export class ProductService {
  private productUrl = environment.apiUrl+'products';

  constructor(private http: HttpClient) {}

  getProducts() {
    return this.http.get<Product[]>(this.productUrl, {responseType: 'json'});
  }

  getProduct(id: number) {
    return this.http.get<Product>(this.productUrl + '/' + id, {responseType: 'json'});
  }

  getActiveCategories() {
    return this.http.get(this.productUrl + '/categories', {responseType: 'json'});
  }

  getAllCategories() {
    return this.http.get(this.productUrl + '/allCategories', {responseType: 'json'});
  }

  addProduct(product: Product) {
    return this.http.post(this.productUrl + '/addProduct', product, {responseType: 'json'});
  }

  editProduct(product: Product) {
    return this.http.put(this.productUrl + '/updateProduct', product, {responseType: 'json'})
  }

  deleteProduct(id: number) {
    return this.http.delete(this.productUrl + '/' + id, {responseType: 'json'})
  }

  uploadProductImage(file: File) {
    const formData: FormData = new FormData();
    formData.append('image', file, file.name)
    return this.http.post(this.productUrl + '/uploadimage', formData, {responseType: 'json'})
  }
}


