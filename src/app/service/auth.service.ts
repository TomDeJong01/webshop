import {Injectable, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UserModel} from '../models/user.model';
import {BehaviorSubject, Observable} from 'rxjs';
import { environment } from '../../environments/environment';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class AuthService implements OnInit{
  private url = environment.apiUrl;
  private isAuthenticatedObservable = new BehaviorSubject<boolean>(false);
  private isAdminObservable = new BehaviorSubject<boolean>(false);

  constructor(private router: Router, private http: HttpClient, private route: ActivatedRoute, private cookieService: CookieService) { }
  ngOnInit() {
  }

  login(email: string, password: string) {
    this.logOut();
    const encodedPassword = btoa(password.toString());
    return this.http.post(this.url + 'auth/signin', {email, password:encodedPassword}, {responseType: 'json'})
  }

  initUser(token: string) {
    this.cookieService.set('auth-token', token)
    this.isAuthenticatedObservable.next(true)
    this.isAdminObservable.next(this.getAttributeFromToken('is_admin'))
  }

  getIsAuthenticatedObservable(): Observable<boolean> {
    if(this.cookieService.get('auth-token') !== '') {
      this.initUser(this.cookieService.get('auth-token'))
    }
    return this.isAuthenticatedObservable.asObservable();
  }
  getIsAdminObservable(): Observable<boolean> {
    return this.isAdminObservable.asObservable();
  }

  public isAuthenticated(): boolean {
    return this.isAuthenticatedObservable.getValue();
  }

  public getAttributeFromToken(tokenAttribute: string)  {
    if(this.cookieService.get('auth-token') !== null) {
      let splitToken = this.cookieService.get('auth-token').split('.')[1]
      let jwtData = JSON.parse(window.atob(splitToken));
      return jwtData[tokenAttribute]
    }
    return false;
  }

  logOut() {
    this.isAuthenticatedObservable.next(false)
    this.isAdminObservable.next(false)
    this.cookieService.delete('auth-token');
  }

  addUser(user: UserModel) {
    return this.http.post(this.url + 'auth/signup', user, {responseType: 'json'});
  }
  validateToken() {
    console.log("validate token ")
    if(this.cookieService.get('auth-token') === '') {
      return null;
    }
    this.http.get(this.url + 'auth/validatetoken', {responseType: 'json'}).subscribe(response => {
      if(response['status'] === 'success') {
        this.isAuthenticatedObservable.next(true)
        this.isAdminObservable.next(this.getAttributeFromToken('is_admin'))
      } else {
        this.logOut()
      }
    })
  }
}
