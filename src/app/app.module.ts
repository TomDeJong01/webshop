import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { ShopComponent } from './components/shop/shop.component';
import { ProductComponent } from './components/shop-product/product.component';
import {AppRoutingModule} from './app-routing';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProductService} from './service/product.service';
import {AuthInterceptor} from './auth.interceptor';
import {CommonModule} from '@angular/common';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import {ShoppingCartService} from './service/Shopping-cart.service';
import { LoginComponent } from './components/login/login.component';
import {AuthService} from './service/auth.service';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { RegisterComponent } from './components/register/register.component';
import { AdminComponent } from './components/admin/admin.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { AdminProductComponent } from './components/admin-product/admin-product.component';
import { CookieService} from 'ngx-cookie-service';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ShopComponent,
    ProductComponent,
    ShoppingCartComponent,
    LoginComponent,
    UserDetailComponent,
    RegisterComponent,
    AdminComponent,
    ProductFormComponent,
    AdminProductComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatSelectModule,
    MatFormFieldModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor , multi: true },
    ProductService,
    CookieService,
    ShoppingCartService,
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

