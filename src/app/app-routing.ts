import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShopComponent} from './components/shop/shop.component';
import {HomeComponent} from './components/home/home.component';
import {ShoppingCartComponent} from './components/shopping-cart/shopping-cart.component';
import {LoginComponent} from './components/login/login.component';
import {UserDetailComponent} from './components/user-detail/user-detail.component';
import {RegisterComponent} from './components/register/register.component';
import {AdminComponent} from './components/admin/admin.component';
import {ProductFormComponent} from './components/product-form/product-form.component';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'shop', component: ShopComponent},
  {path: 'shopping-cart', component: ShoppingCartComponent},
  {path: 'about', component: HomeComponent},
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent },
  {path: 'users/:username', component: UserDetailComponent},
  {path: 'secretAdminStuff', component: AdminComponent},
  {path: 'secretAdminStuff/addProduct', component: ProductFormComponent},
  {path: 'secretAdminStuff/editProduct/:id', component: ProductFormComponent},
  {path: '**', redirectTo: '/not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
