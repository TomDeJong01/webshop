export class UserModel {
  email: string;
  password: string;
  zipcode: string;
  street: number;
  house_nr: string;
  place: string;
  isAdmin: boolean;
  userId?: number;

  public constructor(values?: Partial<UserModel>) {
    Object.assign(this, values);
  }
}
