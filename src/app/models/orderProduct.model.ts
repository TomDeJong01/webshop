import {Product} from './product.model';

export class OrderProduct {
  product: Product
  amount: number;

  public constructor(product: Product, amount: number) {
    this.product = product;
    this.amount = amount;
  }
}
