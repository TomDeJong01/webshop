export class OrderModel {
  order_id: number
  user_id: number;
  order_status: string;
  order_products= [];

  public constructor(order_id: number, user_id: number, order_status: string) {
    this.order_id = order_id;
    this.user_id = user_id;
    this.order_status = order_status;
  }

  addOrderProduct(product_name: string, amount: number) {
    this.order_products.push({product_name, amount});
  }
}
