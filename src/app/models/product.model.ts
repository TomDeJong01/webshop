export class Product {
  id: number;
  name: string;
  category: string;
  category_id: number;
  price: number;
  alcoholpercentage: number;
  fermentation: string;
  brewery: string;
  img: string;

  public constructor(values?: Partial<Product>) {
    Object.assign(this, values);
  }
}
