import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../models/product.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.scss']
})
export class AdminProductComponent implements OnInit {
  @Input() product: Product;

  constructor(private router: Router, private route: ActivatedRoute  ) { }

  ngOnInit() {
  }

  onEdit() {
    this.router.navigate(['editProduct/' + this.product.id], {relativeTo: this.route})
  }
}
