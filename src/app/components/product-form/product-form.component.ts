import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Product} from '../../models/product.model';
import {ProductService} from '../../service/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {inputNames} from '@angular/cdk/schematics';


@Component({
  selector: 'app-add-product',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  addProductForm: FormGroup;
  public product = new Product();
  public categories = [];
  public editMode = false;
  public id: number;
  fileToUpload: File = null;
  public categorySelected: string;
  newCategory = false;
  categoryFormControl = new FormControl('')

  public categoryObject: {id: number, category: string} = {id: -1, category: "newCategory"}




  constructor(private shopService: ProductService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.addProductForm = new FormGroup({
      'name': new FormControl(null, Validators.required ),
      'price': new FormControl(null, [Validators.required, Validators.pattern(/^(\d+\.\d{1,2})/)]),
      'category': new FormControl(null, ),
      'newCategoryName': new FormControl(null, ),
      'brewery': new FormControl(null, []),
      'img': new FormControl(null, ),
    });
    // this.addProductForm.addControl('category', this.categoryObject);
    this.shopService.getAllCategories().subscribe(response => {
      this.categories = response["data"];
    })

    if (this.route.snapshot.params['id'] > 0 ) {
      this.editMode = true;
      this.id = this.route.snapshot.params['id']
      this.categoryObject.id = this.product.category_id;
      this.categoryObject.category = this.product.category
      this.shopService.getProduct(this.id).subscribe(response => {
        this.product = response["data"];
        this.product.category_id = response["data"]["category_id"]
        console.log(response["data"])
        this.addProductForm.controls['name'].setValue(this.product.name);
        this.addProductForm.controls['price'].setValue(this.product.price);
        this.addProductForm.controls['category'].setValue(this.categoryObject);
        this.addProductForm.controls['brewery'].setValue(this.product.brewery);
        this.addProductForm.controls['img'].setValue(this.product.img);
        // this.category = {id: this.product.category_id, category: this.product.category}
      })
      console.log(this.product);
    }
    console.log("done init")
  }



  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onSubmit(formGroup: FormGroup) {
    if(formGroup.valid) {
      this.submitForm(formGroup, this.editMode)
    }
  }

  onCancel() {
    console.log(this.product)
    console.log(this.addProductForm.controls['category'].value.toString())
    // this.router.navigate(['/secretAdminStuff'])
  }

  isEditmode() {
    return this.editMode;
  }

  onDeleteProduct() {
    this.shopService.deleteProduct(this.product.id).subscribe();
    this.onCancel();
  }

  private submitForm(formGroup: FormGroup, editMode: boolean) {
    this.product.name = formGroup.value.name;
    this.product.price = formGroup.value.price;
    this.product.brewery = formGroup.value.brewery;
    if(this.newCategory) {
      this.product.category = formGroup.value.category.category
    } else {
      this.product.category_id = formGroup.value.category.id
    }
    if(this.fileToUpload !== null) {
      this.product.img = this.fileToUpload.name;
      this.shopService.uploadProductImage(this.fileToUpload).subscribe(res => {})
    }
    if(editMode) {
      this.product.id = this.id;
      console.log(this.product)
      // this.shopService.editProduct(this.product).subscribe( res => {
        // TODO msg: update completed;
      // });
    } else {
      console.log(this.product)
      // this.shopService.addProduct(this.product).subscribe();
      // this.shopService.addProduct(this.shop-product).subscribe(data => { response status = data["status"]});
    }
    // this.onCancel();

  }

  dropdownSelectionChange(e) {
    console.log(e.value)
    if(e.value.id === 0) {
      this.newCategory = true;
    } else {
      this.newCategory = false;
      this.addProductForm.controls['category_id'].setValue(e.value.id)
      this.addProductForm.controls['category'].setValue(e.value.category)
      this.product.category_id = e.value.id;
    }
  }
}
