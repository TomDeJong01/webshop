import {Component, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ShoppingCartService} from '../../service/Shopping-cart.service';
import {OrderProduct} from '../../models/orderProduct.model';
import {CookieService} from 'ngx-cookie-service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy, OnChanges {
  public isAuthenticatedObservable: boolean;
  public isAdminObservable: boolean;
  public shoppingCartItems: OrderProduct[];
  private shoppingCartProducts = '';

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute,
              private shoppingCartService: ShoppingCartService, private cookieService: CookieService) {
    this.authService.getIsAuthenticatedObservable().subscribe(val => this.isAuthenticatedObservable = val);
    this.authService.getIsAdminObservable().subscribe(val => this.isAdminObservable = val);
    // if(!this.isAuthenticatedObservable && localStorage.getItem('Token') !== null) {
    if(!this.isAuthenticatedObservable && cookieService.get('auth-token') !== null) {
      this.authService.validateToken();
    }
  }

  ngOnInit() {
    this.shoppingCartService.loadCookie()
    this.shoppingCartService.getShoppingCartObservable().subscribe(val => this.shoppingCartItems = val);
    this.shoppingCartProducts = this.shoppingCartItems.length > 0 ? '' : '(' +this.shoppingCartItems +')'
  }

  onLogOut() {
    this.authService.logOut();
    this.router.navigate(['../home/'], {relativeTo: this.route});
  }

  ngOnDestroy() {}

  ngOnChanges() {}

  getUserName() {
    return this.authService.getAttributeFromToken('email')
  }

}
