import { Component, OnInit } from '@angular/core';
import { ProductService} from '../../service/product.service';
import {Product} from '../../models/product.model';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  public products: Product[];
  public product: Product;
  public categories = [];
  public checkboxes: {id: number, category: string, value: boolean}[] = [];


  constructor(private shopService: ProductService) {
    this.shopService.getProducts().subscribe(result => {
        this.products = result['data'];
        console.log(this.products)
      });
    this.shopService.getActiveCategories().subscribe(response => {
      response["data"].forEach((val) => {
        this.categories.push(val["category"])
        this.checkboxes.push({id: val["category_id"], category: val["category"], value: false})
      })
    })
  }

  ngOnInit() {

  }

  selectedCheckboxes() {
    return this.checkboxes.filter(checkbox => checkbox.value === true);
  }

  activeCategory(productCategory: string) {
    if (this.selectedCheckboxes().some( checkbox => checkbox.category === productCategory)) {
      return true
    }
  }

  filterActive() {
    if (this.selectedCheckboxes().some(checkbox => checkbox.value === true)) {
      return true;
    }
  }
}

