import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../service/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  errorMessage = '';

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required),
    });
  }

  onSubmit(form: FormGroup) {
    this.authService.login(form.value.email, form.value.password).subscribe(response => {
      this.authService.initUser(response['data'])
      this.router.navigate(['../users/'+form.value.email], {relativeTo: this.route});
    }, error => {
      this.errorMessage = 'Password is incorrect'
    });
  }
}
