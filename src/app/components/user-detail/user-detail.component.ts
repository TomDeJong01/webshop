import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserModel} from '../../models/user.model';
import {AuthService} from '../../service/auth.service';
import {HttpClient} from '@angular/common/http';
import {ShoppingCartService} from '../../service/Shopping-cart.service';
import {OrderModel} from '../../models/Order.model';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  public orders = [];
  public displayOrderProducts = [];
  public user: UserModel = new UserModel();

  constructor(private authService: AuthService, private shoppingcartService: ShoppingCartService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    if(this.authService.isAuthenticated() && this.route.snapshot.params["username"] === this.authService.getAttributeFromToken("email")) {
      this.user.email = this.authService.getAttributeFromToken("email")
      this.getOrders()
    } else {
      this.router.navigate(["/home"])
    }
  }

  getOrders() {
    this.shoppingcartService.getOrders(this.user.email).subscribe(response => {
      response["data"].forEach(item => {
        if(this.orders.some(order => order.order_id === item.order_id)) {
          this.orders.find(order => order.order_id === item.order_id).addOrderProduct(item.name, item.amount);
        } else {
          this.orders.push(new OrderModel(item.order_id,this.authService.getAttributeFromToken("user_id"), item.order_status))
          this.orders.find(order => order.order_id === item.order_id).addOrderProduct(item.name, item.amount);
        }
      })
    })
  }

  ngOnDestroy(): void {}

  toggleOrderProducts(order_id: number) {
    document.getElementById('orderProducts' + String(order_id)).hidden = !document.getElementById('orderProducts' + String(order_id)).hidden
  }
}
