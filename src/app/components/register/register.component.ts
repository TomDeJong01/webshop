import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserModel} from '../../models/user.model';
import {AuthService} from '../../service/auth.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public user = new UserModel();
  public regForm: FormGroup;
  public errorMessage: string;

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  this.regForm = new FormGroup({
    'password': new FormControl(null, Validators.required),
    'email': new FormControl(null, [Validators.required, Validators.email]),
    'zipcode': new FormControl(null, [Validators.required]),
    'street': new FormControl(null, Validators.required),
    'house_nr': new FormControl(null, Validators.required),
    'place': new FormControl(null, Validators.required),
  });

  }

  onSubmit(form: FormGroup) {
    if (form.valid ) {
      this.user = new UserModel(this.regForm.value);
      this.user.password = btoa(this.user.password);
      this.authService.addUser(this.user).subscribe(response => {
        this.authService.initUser(response["data"])
        this.router.navigate(['../users/'+form.value.email], {relativeTo: this.route});
      }, err => {
        this.errorMessage = err.error.error;
        }
      );
    }
  }
}
