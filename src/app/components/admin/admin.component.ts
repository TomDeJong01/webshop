import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../service/product.service';
import {Product} from '../../models/product.model';
import {AuthService} from '../../service/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public products: Product[];
  private isAdminObservable: boolean;

  constructor(private shopService: ProductService, private authService: AuthService, private router: Router, private route: ActivatedRoute) {
    this.authService.getIsAdminObservable().subscribe(val => this.isAdminObservable = val);
  }

  ngOnInit() {
    if(!(this.isAdminObservable)){
      this.router.navigate(['/home'])
    }

    this.shopService.getProducts()
      .subscribe(result => {
        this.products = result["data"];
        console.log(this.products)
      });
  }

  onAddProduct() {
    this.router.navigate(['addProduct'], {relativeTo: this.route})
  }
}
