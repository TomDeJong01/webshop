import {Component, OnInit} from '@angular/core';
import {ShoppingCartService} from '../../service/Shopping-cart.service';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';
import {OrderProduct} from '../../models/orderProduct.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  public shoppingCartItems: OrderProduct[];
  public totalAmount;
  public totalAmountString: string;

  constructor(private shoppingCartService: ShoppingCartService, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.shoppingCartService.loadCookie();
    this.shoppingCartService.getShoppingCartObservable().subscribe(val => this.shoppingCartItems = val);
    this.updateTotal();
  }

  updateTotal() {
    let newPrice = 0;
    this.shoppingCartItems.forEach((item => {
      newPrice = newPrice + (item.product.price * +item.amount)
    }))
    this.totalAmount = Number.parseFloat(String(newPrice)).toFixed(2)
    this.totalAmountString = String(this.totalAmount).replace('.', ',')
  }

  //TODO wat doet deze hier??
  onRegister() {
    this.router.navigate(['/register'])
  }
  onLogin() {
    this.router.navigate(['/login'])
  }

  onOrder() {
    this.shoppingCartService.placeOrder(this.shoppingCartItems).subscribe(response => {
      if(response["status"] === "success") {
        this.shoppingCartService.emptyShoppingCart();
        this.router.navigate(['/users/' + this.authService.getAttributeFromToken("email")])
      } else {
        //TODO display error
        //display error;
      }
    });
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  removeOrderProduct(productId) {
    this.shoppingCartService.removeFromShoppingcart(productId)
    this.updateTotal();
  }

  priceFixedDigits(price) {
    return String(price).replace('.', ',')
  }
}
