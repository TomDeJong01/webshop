import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../models/product.model';
import {ProductService} from '../../service/product.service';
import {OrderProduct} from '../../models/orderProduct.model';
import {ShoppingCartService} from '../../service/Shopping-cart.service';

@Component({
  selector: 'shop-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {
  @Input() product: Product;
  @Input() index: number;
  public amount = 1;
  public productPriceString: string;
  constructor(private productService: ProductService, private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
    this.productPriceString = String(this.product.price).replace('.', ',')
    // this.productPriceString = "123"

  }

  onAdd(product: Product) {
    console.log(product)
    this.product = product;
    this.amount.toFixed();
    if(!isNaN(this.amount)) {
      this.amount = Math.round(this.amount)
    }
    if(this.amount <= 0) {
      alert('cannot order less then 1 of a product')
    } else {
      const orderProduct = new OrderProduct(product, this.amount);
      this.shoppingCartService.addToShoppingcart(orderProduct);
    }
  }

  numberOnly(event: KeyboardEvent): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
}
