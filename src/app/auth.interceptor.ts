import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from './service/auth.service';
import {catchError} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private cookieService: CookieService) {  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {token: `${this.cookieService.get('auth-token')}`}
    });

    return next.handle(request).pipe(catchError(catchErrorSelector => {
      if ([401, 403].indexOf(catchErrorSelector.status) !== -1) {
        console.log('log auth interceptor error');
        console.log(catchErrorSelector);
        this.authService.logOut();
      }
      return throwError(catchErrorSelector);
    }));
  }
}
